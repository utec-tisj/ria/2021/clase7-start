import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GatosComponent } from './gatos/gatos.component';
import { PerrosComponent } from './perros/perros.component';

const routes: Routes = [
  {
    path: 'gatos', component: GatosComponent
  },
  {
    path: 'perros', component: PerrosComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
